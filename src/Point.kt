fun main(){
    val c1 = Point(4,5)
    val c2 = Point(6,5)
    println(c1.toString())
    println(c1.equals(c2))
    println("simetriuli satavis mimart " + -c1)
}

data class Point(val x: Int, val y: Int) {
    init {
        println("x and y coord - $x,$y")
    }

    override fun equals(other: Any?): Boolean {
        if(other is Point){
            return x == other.x && y == other.y
        }
        return false
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }

}
operator fun Point.unaryMinus() = Point(-x, -y)